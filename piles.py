import numpy as np
import random

numberOfPiles = 100
maxIter = 100000000

piles = np.empty(numberOfPiles)
piles.fill(numberOfPiles)
totalPiles = np.sum(piles)
numPiles = numberOfPiles
x = 0
while (x < maxIter and piles.size > 1):
  piles = piles[np.nonzero(piles)]
  # At each turn pick a coin from each pile, and
  # move it into a randomly selected pile.
  for i in np.arange(piles.size):
    piles[i] -= 1
    num = np.random.randint(piles.size)
    piles[num] += 1
  numPiles = piles.size
  x += 1


print("The number of piles remaining are: ", numPiles)

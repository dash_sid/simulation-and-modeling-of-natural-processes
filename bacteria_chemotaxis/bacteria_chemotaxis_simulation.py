from __future__ import print_function, division
import random
import math
import numpy as np
import matplotlib.pyplot as plt


class Bacteria(object):
	""" Defines the movement of bacteria. """

	def __init__(self, x, y):
		self.x = x
		self.y = y
		self.vx = None
		self.vy = None
		self.randomize_velocity()
		self.old_density = get_density_version_A(self.x, self.y)

	def randomize_velocity(self):
		alpha = random.random() * math.pi * 2
		self.vx = math.cos(alpha) * V
		self.vy = math.sin(alpha) * V
		assert(math.hypot(self.vx, self.vy) - V) < 1e-7

	def update(self):
		current_density = get_density_version_A(self.x, self.y)
		go_forward = False
		if current_density > self.old_density:
			if random.random() < P1:
				go_forward = True
		else:
			if random.random() < P2:
				go_forward = True
		if not go_forward:
			self.randomize_velocity()

		self.x += self.vx * DT
		self.y += self.vy * DT

		# Domain Periodicity.
		self.x %= L
		self.y %= L
		self.old_density = current_density


def get_density_version_A(x, y):
	return 1/(1+math.hypot(x-L/2, y-L/2))


def get_density_version_B(x, y):
	return float(math.hypot(x - L/2.0, y-L/2.0) < 15e-6)


def draw(b_list, n, t):
	m = np.zeros((n, n))
	for x in range(n):
		for y in range(n):
			m[x, y] = get_density_version_A(x*L/n, y*L/n)

	for bacteria in b_list:
		x, y = int(bacteria.x*n/L), int(bacteria.y*n/L)
		m[x, y] = 1.0

	# Add interpolation = 'None' for non-smoothed image
	plt.imshow(m)
	plt.savefig("bacteria" + str(t) + ".png")
	# plt.show()


#################### MAIN ##########################
V = 2e-6
DT = 0.2
L = 100e-6
P1 = 0.9
P2 = 0.5
N = 10

b_list = [Bacteria(random.random()*L, random.random()*L) for i in range(N)]

for t in range(1000):
	if t % 10 == 0:
		print("Step:", t)
		draw(b_list, 100, t)
	for bacteria in b_list:
		bacteria.update()

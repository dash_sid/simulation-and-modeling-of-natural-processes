# Simple Python implementation of a Barnes-Hut galaxy simulator.
# This file is part of the exercise series of the University of Geneva's
# MOOC "Simulation and Modeling of Natural Processes".

import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from mpl_toolkits.mplot3d import Axes3D


class Node:
	""" A node represents a body if it is an end node (i.e. if node.child is None),
			or an abstract node of the quad-tree if it has child.
	"""

	def __init__(self, mass, x, y):
		""" Initializes a child-less node (a node representing a body).
				Instead of storing the position of a node, stores the mass * position.
				This makes it easier to update the center-of-mass.
		"""
		self.mass = mass
		self.m_position = mass * np.array([x, y])
		self.momentum = np.array([0., 0.])
		self.child = None

	def into_next_quadrant(self):
		""" Places node into the next-level quadrant and returns the quadrant number. """
		# s: side-length of the current quadrant.
		self.side_length = 0.5 * self.side_length
		return self._subdivide(1) + 2 * self._subdivide(0)

	def position(self):
		""" Returns the physical position of node independent of currently active quadrant. """
		return self.m_position / self.mass

	def reset_to_0th_quadrant(self):
		""" Repositions the node to the level-0 quadrant (full domain). """
		# Side-length of the level-0 quadrant is 1.
		self.side_length = 1.0
		# Relative position inside the quadrant is equal to physical position.
		self.relative_position = self.position().copy()

	def distance(self, other_node):
		""" Returns the distance between the current node and the other node. """
		return np.linalg.norm(other_node.position() - self.position())

	def force_exterted_on(self, other_node):
		""" Returns the force exterted by the current node on a given body. """
		# Introduce a small cut-off distance to avoid numerical instabilities.
		cutoff_distance = 2e-3
		distance = self.distance(other_node)
		if distance < cutoff_distance:
			return np.array([0., 0.])
		else:
			# Return gravitational force
			return (self.position() - other_node.position()) * (self.mass * other_node.mass / distance**3)

	def _subdivide(self, i_vect):
		""" Places node into the next-level quadrant along direction i_vect and recomputes 
				the relative position of the node inside this quadrant. 
		"""
		self.relative_position[i_vect] *= 2.0
		if self.relative_position[i_vect] < 1.0:
			quadrant = 0
		else:
			quadrant = 1
			self.relative_position[i_vect] -= 1.0
		return quadrant


def add_node(body, node):
	""" Barnes-Hut algorithm: Creation of the quad-tree. This function adds a new body 
			into a quad-tree node. Returns an updated version of the node.
	"""
	# To limit the recursion depth, set a lower limit for the size of the quadrant
	smallest_quadrant = 1e-4
	# If the node n does not contain a body, put the new body b here.
	new_node = body if node is None else None
	if node is not None and node.side_length > smallest_quadrant:
		# If node n is an external node, then the new body b is in conflict with
		# a body already in this region.
		if node.child is None:
			new_node = deepcopy(node)
			# Subdivide the region further by creating four children.
			new_node.child = [None for i in range(4)]
			# Insert the already existing body recursively into the appropriate quadrant.
			quadrant = node.into_next_quadrant()
			new_node.child[quadrant] = node
		else:
			# If node n is an internal node, no need to modify its child.
			new_node = node

		# If node n is or has become an internal node, update its mass and center of mass.
		new_node.mass += body.mass
		new_node.m_position += body.m_position

		# Recursively add the new body into the appropriate quadrant.
		quadrant = body.into_next_quadrant()
		new_node.child[quadrant] = add_node(body, new_node.child[quadrant])
	return new_node


def compute_net_force(body, node, theta):
	""" Barnes-Hut algorithm: Usage of the quad-tree. This function computes the net force 
			on a body exerted by all bodies in the node.
	"""
	# If the current node is an external node, calculate the force exerted by the current node on the body.
	if node.child is None:
		return node.force_exterted_on(body)

	# Else, if the current node is an internal node, calculate the ratio s/d.
	# If s/d < θ, treat this internal node as a single body, and calculate the force it exterts on the body.
	if node.side_length / node.distance(body) < theta:
		return node.force_exterted_on(body)

	# Else, run the procedure recursively on each child.
	return sum(compute_net_force(body, c, theta) for c in node.child if c is not None)


def verlet_integration(bodies, root, theta, G, dt):
	""" Executes a time iteration according to the verlet algorithm. """
	for body in bodies:
		force = G * compute_net_force(body, root, theta)
		# If the net force applied to a body is constant for a time interval dt, momentum = dt * force.
		body.momentum += dt * force
		body.m_position += dt * body.momentum


def plot_bodies(bodies, i):
	""" Helper function to plot the current position of the bodies. """
	ax = plt.gcf().add_subplot(111, aspect='equal')
	ax.cla()
	ax.scatter([b.position()[0] for b in bodies], [b.position()[1] for b in bodies], 1)
	ax.set_xlim([0., 1.0])
	ax.set_ylim([0., 1.0])
	plt.gcf().savefig('bodies2D_{0:06}.png'.format(i))


######### MAIN PROGRAM ########################################################
# Theta-criterion of the Barnes-Hut algorithm.
theta = 0.5
# Mass of the body.
mass = 1.0
# Initial radius of the circle in which the bodies are distributed.
initial_radius = 0.1
# Initial maximum velocity of the bodies.
initial_max_velocity = 0.1
# Gravitational constant.
G = 4e-6
# Discrete time step.
dt = 1e-3
# Number of bodies (smaller value because all bodies outside the initial radius are removed).
nbodies = 10**3
# Number of time iterations executed by the program.
max_iter = 10**4
# Time-step at which PNG images are written.
img_iter = 20

# The pseudo-random number generator is initialized at a deterministic value for proper validation of the output.
# x and y positions are initialized to a square with side-length = 2*initial_radius.
np.random.seed(1)
posx = np.random.random(nbodies) * 2 * initial_radius + 0.5 - initial_radius
posy = np.random.random(nbodies) * 2 * initial_radius + 0.5 - initial_radius

# Only keep bodies inside a circle of radius initial_radius.
bodies = [Node(mass, px, py) for (px, py) in zip(posx, posy)
          if (px-0.5)**2 + (py-0.5)**2 < initial_radius**2]

# Initially, the bodies have a radial velocity of an amplitude proportional to the distance from the center.
# This induces a rotational motion creating a "galaxy-like" impression.
for body in bodies:
	r = body.position() - np.array([0.5, 0.5])
	body.momentum = np.array([-r[1], r[0]]) * mass * \
            initial_max_velocity * np.linalg.norm(r) / initial_radius

# Prinicipal loop over time iterations.
for i in range(max_iter):
	# The quad-tree is recomputed at each iteration.
	root = None
	for body in bodies:
		body.reset_to_0th_quadrant()
		root = add_node(body, root)
	# Computation of forces, and advancement of bodies.
	verlet_integration(bodies, root, theta, G, dt)
	print(bodies[0].position())

	if i % img_iter == 0:
		print("Writing images at iteration {0}".format(i))
		plot_bodies(bodies, i//img_iter)

	firstBody = bodies[0]
	print(i, ":", firstBody.m_position[1])

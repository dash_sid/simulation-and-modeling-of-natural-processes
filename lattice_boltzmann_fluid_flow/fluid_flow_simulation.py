# 2D flow around a cylinder

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

# -------------------- Flow parameters --------------------

# Total number of time iterations
maxIter = 2*10**5
# Reynolds number
Re = 10.0
# Number of lattice nodes
nx, ny = 420, 180
# Height of the domain in lattice units
ly = ny - 1
# Coordinates of the cylinder
cx, cy, r = nx//4, ny//2, ny//9
# Velocity in lattice units
uLB = 0.04
# Viscosity in lattice units
nulb = uLB * r / Re
# Relaxation parameter
omega = 1 / (3 * nulb + 0.5)

# -------------------- Lattice Constants --------------------

# Lattice velocities - v0 to v8
v = np.array([[1, 1], [1, 0], [1, -1], [0, 1], [0, 0],
              [0, -1], [-1, 1], [-1, 0], [-1, -1]])

# Lengths of velocities of each lattice
t = np.array([1/36, 1/9, 1/36, 1/9, 4/9, 1/9, 1/36, 1/9, 1/36])

# Inflow Condition
# Indices of the columns of lattice populations - f0 to f8
col1 = np.array([0, 1, 2])
col2 = np.array([3, 4, 5])
col3 = np.array([6, 7, 8])


# -------------------- Function Definitions --------------------

def macroscopic(fin):
	""" Return values of macroscopic parameters of the input populations.

			Parameters:
			rho: lattice density
			u: macroscopic velocity
			v: lattice velocity
	"""
	rho = np.sum(fin, axis=0)
	u = np.zeros((2, nx, ny))					# 2-D Vector
	for i in range(9):								# Range is the total number of populations
		u[0, :, :] += v[i, 0] * fin[i, :, :]
		u[1, :, :] += v[i, 1] * fin[i, :, :]
	u /= rho
	return rho, u


def equilibrium(rho, u):
	""" Equilibrium Distribution Function. """
	usqr = 3/2 * (u[0]**2 + u[1]**2)
	feq = np.zeros((9, nx, ny))
	for i in range(9):
		cu = 3 * (v[i, 0]*u[0, :, :] + v[i, 1]*u[1, :, :])
		feq[i, :, :] = rho*t[i] * (1 + cu + 0.5*cu**2 - usqr)
	return feq


def obstacle(x, y):
	""" Creation of a mask with 1/0 values, defining the shape of the obstacle. """
	return (x-cx)**2 + (y-cy)**2 < r**2


def inivel(d, x, y):
	""" Velocity inlet with perturbation.
			Initial velocity profile: almost zero, with a slight perturbation to trigger the instability.
	"""
	return (1-d) * uLB * (1 + 1e-4 * np.sin(y / ly*2*np.pi))


# Setup: Cylindrical obstacle and velocity inlet with perturbation
obstacle = np.fromfunction(obstacle, (nx, ny))
velocity = np.fromfunction(inivel, (2, nx, ny))

# Initialization of populations at equilibrium with the given velocity
fin = equilibrium(1, velocity)

for time in range(maxIter):
	# Right wall: Outflow condition
	fin[col3, -1, :] = fin[col3, -2, :]

	# Compute macroscopic variables, density and velocity
	rho, u = macroscopic(fin)
	# print("Macroscopic Density:", rho)
	# print("Macroscopic velocity:", u)

	# Left wall: Inflow condition
	u[:, 0, :] = velocity[:, 0, :]
	rho[0, :] = 1/(1-u[0, 0, :]) * (np.sum(fin[col2, 0, :],
                                        axis=0) + 2*np.sum(fin[col3, 0, :], axis=0))

	# Compute Equilibrium
	feq = equilibrium(rho, u)
	fin[[0, 1, 2], 0, :] = feq[[0, 1, 2], 0, :] + \
		fin[[8, 7, 6], 0, :] - feq[[8, 7, 6], 0, :]

	# Collision Step
	fout = fin - omega * (fin - feq)

	# Bounce-back condition for the obstacle
	for i in range(9):
		fout[i, obstacle] = fin[8-i, obstacle]

	# Streaming step
	for i in range(9):
		fin[i, :, :] = np.roll(
			np.roll(fout[i, :, :], v[i, 0], axis=0), v[i, 1], axis=1)

	# Visualization of the velocity
	if (time % 100 == 0):
		plt.clf()
		plt.imshow(np.sqrt(u[0]**2 + u[1]**2).transpose(), cmap=cm.Reds)
		plt.title('Re={0}'.format(Re))
		plt.savefig("velocity.{0:04d}.png".format(time//100))

# Simulation and Modeling of Natural Processes by University of Geneva #

This project is based on the course provided by University of Geneva.

https://www.coursera.org/learn/modeling-simulation-natural-processes

This repository contains self-made notes and codes that cover topics related to fluid mechanics, Monte-Carlo methods, Euler's methods, Navier-Stokes equation, bio-medical modeling, 
HPC, parallelism, dynamical systems, cellular automata, boundary conditions, Lattice Boltzmann method, computational fluid dynamics, n-body problem, stellar bodies, molecular dynamics, 
discrete events simulation, classical (Newtonian) mechanics, and agent-based modeling.


### Setup ###

* Python3 - Numpy, Matplotlib, 
* Imagemagick or FFMPEG - to convert images to animated gifs for simulation

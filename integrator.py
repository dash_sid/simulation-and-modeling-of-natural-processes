import numpy as np
import math

class Integrator:
	""" Compute numerical integration using mid-point rule. """
	def __init__(self, xMin, xMax, N):
		self.xMin = xMin
		self.xMax = xMax
		self.N = N
		self.total = 0

	def integrate(self):
		deltaX = (self.xMax - self.xMin) / self.N
		for i in range(self.N):
			xI = self.xMin + i * deltaX
			fX = xI**2 * np.exp(-xI) * np.sin(xI)
			self.total += fX * deltaX
		return self.total

	def show(self):
		print("Integration result = {:.5f}".format(self.total))
		print(round(self.total, 5))

examp = Integrator(1,3,200000)
examp.integrate()
examp.show()
